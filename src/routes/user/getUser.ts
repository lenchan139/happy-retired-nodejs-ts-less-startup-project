import * as express from 'express'
export async function getUser(req: express.Request, res: express.Response, next: express.NextFunction){
    let id = parseInt(req.params.id)
    if(id && id == NaN) id = -1
    res.render('user/getUser', {
        title: 'Express',
        id: id
    });
}