'use strict';

import * as express from 'express';
import * as indexRoutes from './index'
import * as userRoutes from './user'
const router = express.Router();

router

/* GET home page. */
.get('/', indexRoutes.getMain)

// get user 
.get('/user/:id', userRoutes.getUser)
export default router;